# build-test-jdim
このプロジェクトはJDimのビルド検証用プロジェクトです。   
Dockerでx86の各ディストリ環境を用意してビルドを検証しています。   
結果は以下のスプレットシートに新たなシートとして追記されます。
https://docs.google.com/spreadsheets/d/1PmEC9u-BbqQIPzObDYN6ylK3ODO2suhH7App2kVBqTI/edit?usp=sharing
現状ではCIで実行に時間(約2h)がかかるのでーカルでの実行をおすすめします。

## 環境
Dockerとdocker-compose環境を用意してください。   
## Run
```bash
git clone https://gitlab.com/naniwastrongboy/build-test-jdim.git
cd build-test-jdim/testcase
##local-test.shの一行目を編集してテストしたいハッシュ値のものに変えてください。
##あるいは、コメントアウトして都度exportしてもいいかも。
bash local-test.sh
```
ローカルからの実行でもスプレットシートに映されます。

