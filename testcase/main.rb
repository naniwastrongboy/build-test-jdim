require 'net/http'
require 'uri'
require 'json'

h_value = Time.now.to_s
lines=File.read("status")
url = "https://script.google.com/macros/s/AKfycbwY-C4ALrxCx-UaM0NHlTjmBEwBp35W9eHSt0OjtWapmE0ZK8A/exec"
uri = URI.parse(url)
request = Net::HTTP::Post.new(uri)
request.body = JSON.dump({
  "title" => h_value,
  "data" => lines
})

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end

#p response.code
puts response.body
