git clone $JDIMREPO
cd JDim
git checkout -b new-branch $JDIMHASH

echo "---------------" >> /status
gcc --version >> /status
echo "-------" >> /status
g++ --version >> /status
echo "-------" >> /status
cat /etc/lsb-release >> /status
cat /etc/redhat-release >> /status
cat /etc/lsb-release || cat /etc/redhat-release || cat /etc/issue >> /status
echo "-------" >> /status
autoreconf -i >> /status
echo "-------" >> /status
./configure  >> /status
echo "-------" >> /status
make >> /status
echo "-------" >> /status
ls -l /JDim/src/jdim >> /status
cat /status >> /tmp/status
