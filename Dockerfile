From ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -y update
RUN apt-get -y install build-essential automake autoconf-archive git libtool
RUN apt-get -y install libgtkmm-3.0-dev libmigemo1 libasound2-data libltdl-dev libasound2-dev libgnutls28-dev

## gcc-6 g++-6
RUN apt-get install gcc-6 g++-6 -y
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 60 --slave /usr/bin/g++ g++ /usr/bin/g++-6

## check it
RUN g++ -v
RUN gcc -v

COPY build.sh build.sh

RUN bash build.sh
